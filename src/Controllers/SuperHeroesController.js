import mongoose from "mongoose";
import * as fs from "fs";

const esquema = new mongoose.Schema({
    nombre: String, habilidades: String,
    raza:String,imagen: String, edad:Number
},{versionKey:false});
const MarvelModel = new mongoose.model('SuperHeroes',esquema);

export const getSuperHeroes = async(req,res)=>{
    try{
        const {id} = req.params;
        const resultado = (id == undefined) 
        ? await MarvelModel.find() : await MarvelModel.findById(id)
        return res.status(200).json({status:true,data:resultado})
    }
    catch(errors){
        return res.status(500).json({status:false,errors:[errors]})
    }
}
export const saveSuperHeroes = async(req,res) =>{
    try{
        const { nombre,habilidades,raza,edad} = req.body
        const valida = validar(nombre,habilidades,raza,edad,req.file,'Y')
        if(valida == ''){
            const newHeroe = new MarvelModel({
                nombre:nombre,habilidades:habilidades,raza:raza,edad:edad, 
                imagen:'/img/'+req.file.filename
            })
            return await newHeroe.save().then(
                () => { res.status(200).json({status:true,message:'Heroe guardado'}) }
            )
        }
        else{
            return res.status(400).json({status:false,errors:valida})
        }
    }
    catch(errors){
        return res.status(500).json({status:false,errors:[errors.message]})
    }
}

export const updateSuperHeroes = async(req,res) =>{
    try{
        const {id} = req.params
        const { nombre,habilidades,raza,edad} = req.body;
        let imagen = ''
        let valores={nombre:nombre,habilidades:habilidades,raza:raza,edad:edad}
        if (req.file!=null){
            imagen = '/img/'+req.file.filename
            valores = {nombre:nombre,habilidades:habilidades,raza:raza,edad:edad,imagen:imagen}
            await eliminarImagen(id)
        }
        const valida = validar(nombre,habilidades,raza,edad,)
        if(valida == ''){
            await MarvelModel.updateOne({_id:id},{$set : valores})
            return res.status(200).json({status:true,message:'Heroe actualizado'})
        }
        else{
            return res.status(400).json({status:false,errors:valida})
        }
    }
    catch(errors){
        return res.status(500).json({status:false,errors:[errors]})
    }
}

export const deleteHeroe = async(req,res) =>{
    try{
        const {id} = req.params
        await eliminarImagen(id)
        await MarvelModel.deleteOne({_id:id})
        return res.status(200).json({status:true,message:'Heroe eliminado'})
    }
    catch(errors) {
        return res.status(500).json({status:false,errors:[errors.message]})
    }
}

const eliminarImagen = async(id) =>{
    const Heroe = await MarvelModel.findById(id)
    const img = Heroe.imagen
    fs.unlinkSync('./public/'+img)
}


const validar = (nombre,habilidades,raza,edad, imagen, validaImg) => {
    const errores = [];
    if(nombre === undefined || nombre.trim() == ''){
        errores.push('El nombre no debe estar vacio');
    }
    if(habilidades === undefined || habilidades.trim() == ''){
        errores.push('Las habilidades no debe estar vacio');
    }
    if(raza === undefined || raza.trim() == ''){
        errores.push('La raza no debe estar vacio');
    }
    if(edad === undefined || edad.trim() == '' || isNaN(edad)){
        errores.push('La edad no debe estar vacio y debe ser numerica');
    }
    if(validaImg == 'Y' && imagen == undefined){
        errores.push('La imagen no debe de estar vacio');
    }
    else{
        if(errores !=''){
            fs.unlinkSync('./public/img/'+imagen.filename);
        }
    }
    return errores;
}