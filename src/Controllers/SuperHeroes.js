import mongoose from "mongoose";
import * as fs from "fs";

const esquema = new mongoose.Schema({
    nombre: String, habilidades: String, 
    raza: String,imagen: String, edad:Number
},{versionKey: false });

const MarvelModel = new mongoose.model('SuperHeros',esquema);

export const getSuperHeroes = async(req,res) =>{
    try {
        const {id}= req.params;
        const resultado = (id == undefined)
        ? await MarvelModel.find() : await MarvelModel.findById(id)
        return res.status(200).json({status:true,results:resultado})
    } 
    
    //pa que no se caiga si hay algún error
    catch (errors) {
        
    }
}

export const saveSuperHeroes = async (req,res) => {
    try{
    const {nombre,habilidades,raza,edad} = req.body;
    const imagen=req.file;
    const valida= validar(nombre,habilidades,raza,edad,imagen,'Y');
    if(valida == ''){
        const newHeroe = new MarvelModel({
        nombre:nombre,habilidades:habilidades,raza:raza,
        edad:edad,imagen: '/img/'+req.file.filename
    });
return await newHeroe.save().then(
    () => res.status(200).json({status:true,message:'Heroe creado con éxito'})    
)
    }
    else{
        return res.status(400).json({status:false,errors:valida});
    }
    }
    catch(errors){
        return res.status(500).json({status:false,errors:[errors.message]})
    }
}

export const updateSuperHero = async (req, res) => {
    try {
        const { id } = req.params;
        const { nombre, habilidades, raza, edad } = req.body;
        let imagen = '';
        let valores = {nombre:nombre, habilidades:habilidades, raza:raza, edad:edad}
        if(req.file != null){                
            imagen = '/img/'+req.file.filename
            valores = {nombre:nombre, habilidades:habilidades, raza:raza, edad:edad,imagen:imagen}
            await eliminarImagen(id);
        }
        const valida = validar(nombre, habilidades, raza, edad);
        if (valida == '') {
            await MarvelModel.updateOne({_id:id},{$set:valores})
            return res.status(200).json({status:true,message:'Heroe actualizado'})    
            
         }
        else{
            return res.status(400).json({status:false,errors:valida});
        }
        }
        catch(errors){
        return res.status(500).json({status:false,errors:[errors.message]})
    }
}


//Eliminar
export const deleteSuperHeroe = async (req, res) => {
    try{
        const {id} = req.params
        await eliminarImagen(id);
        await MarvelModel.deleteOne({_id:id})
        return res.status(200).json({status:true,message:'Héroe eliminado'})     
    }
    catch{
        return res.status(500).json({status:false,errors:[errors]})
    }
}

const eliminarImagen = async(id) => {
    const heroe = await MarvelModel.findById(id);
    const img = heroe.imagen
    fs.unlinkSync('./public/'+img)
}


const validar = (nombre, habilidades, raza, edad, imagen, validarImg) => {
    const errores =[];
    if(nombre === undefined || nombre.trim() == ''){
        errores.push('El nombre no debe de estar vacío');
    }

    if(habilidades === undefined || nombre.trim() == ''){
        errores.push('Las habilidades no deben de estar vacias');
    }

    if(raza === undefined || nombre.trim() == ''){
        errores.push('La raza no debe de estar vacía');
    }

    if(edad === undefined || nombre.trim() == ''){
        errores.push('La edad no debe de estar vacía y debe ser un número');
    }

    if(validarImg == 'Y' && imagen == undefined){ 
        errores.push('La imagen debe ser en formato jpg o png');
    }
    else{
        if(errores != ''){
            fs.unlinkSync('./public/img'+imagen.filename);
        }
    }

    return errores;
}